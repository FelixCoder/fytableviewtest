//
//  ViewController.m
//  FYTableViewCellActionTest
//
//  Created by FelixYin on 2017/6/11.
//  Copyright © 2017年 FelixYin. All rights reserved.
//

#import "ViewController.h"
#import "BGTableViewRowActionWithImage.h"
#import "ScrollViewController.h"
#import "MJRefresh.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)viewDidLayoutSubviews{
    [self.view addSubview:self.tableView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark UITableView DataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID = @"CellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"Row ==== >%@",@(indexPath.row)];
    return cell;
}


- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
//    UITableViewRowAction *rowActionOne = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"           " handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//        NSLog(@"RowAction One");
//    }];
//    rowActionOne.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Reveal - Trash"]];
//    
//    
//    UITableViewRowAction *rowActionTwo = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"            " handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//        NSLog(@"RowAction Two");
//    }];
//    rowActionTwo.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    
    BGTableViewRowActionWithImage *rowActionOne = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDefault title:@"   删除  " backgroundColor:[UIColor brownColor] image:[UIImage imageNamed:@"Copy"] forCellHeight:100 andFittedWidth:80 handler:^(UITableViewRowAction *rowAction, NSIndexPath *index) {
        NSLog(@"RowAction One");
    }];
    
    BGTableViewRowActionWithImage *rowActionTwo = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDefault title:@"   编辑   " backgroundColor:[UIColor redColor] image:[UIImage imageNamed:@"Face"] forCellHeight:100 andFittedWidth:80 handler:^(UITableViewRowAction *rowAction, NSIndexPath *index) {
        NSLog(@"RowAction Two");
    }];
    
    
    return @[rowActionOne,rowActionTwo];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 100;
}


#pragma mark UITableView Delegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ScrollViewController *scrollVC = [[ScrollViewController alloc] init];
    [self.navigationController pushViewController:scrollVC animated:YES];
}


-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        __weak typeof(self) weakSelf = self;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            NSLog(@"下拉");
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // 结束刷新
                [weakSelf.tableView.mj_header endRefreshing];
            });
        }];
        
        _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                // 结束刷新
                [weakSelf.tableView.mj_footer endRefreshing];
            });
            NSLog(@"上拉");
        }];
        
        
    }
    return _tableView;
}

@end

//
//  ScrollViewController.m
//  FYTableViewCellActionTest
//
//  Created by FelixYin on 2017/6/11.
//  Copyright © 2017年 FelixYin. All rights reserved.
//

#import "ScrollViewController.h"
#import "MJRefresh.h"

@interface ScrollViewController ()

@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation ScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillLayoutSubviews{
    [self.view addSubview:self.scrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Lazy Loading

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        CGRect rect = CGRectMake(0, 64, self.view.bounds.size.width, self.view.bounds.size.height - 64);
        _scrollView = [[UIScrollView alloc] initWithFrame:rect];
        _scrollView.backgroundColor = [UIColor whiteColor];
        __weak typeof(self) weakSelf = self;
        _scrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            NSLog(@"下拉");
            [weakSelf.scrollView.mj_header endRefreshing];
        }];
        
        
        _scrollView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            NSLog(@"上拉");
            [weakSelf.scrollView.mj_footer endRefreshing];
        }];
    }
    return _scrollView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
